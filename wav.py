import librosa.core
import os.path as path
import os


def read(path):
    return librosa.core.load(path, sr=11025, mono=True)[0]


def load_all(directory):
    audios = {}
    for (dirpath, dirnames, filenames) in os.walk(directory):
        basename = path.basename(dirpath)
        for file in filenames:
            if file.endswith('.mp3') | file.endswith('.wma'):
                audios[path.join(basename, file)] = read(path.join(dirpath, file))
    return audios
