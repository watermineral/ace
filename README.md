# ACE (Automatic Chord Estimation)#

### Install ###
1. Install [Python3](https://www.python.org/downloads/).  *Note: OSX's `python` terminal command uses Python 2 version. Use `python3` command instead.*
2. Install [pip](https://pip.pypa.io/en/stable/installing/) packet manager. *Note: Again `pip` terminal command uses Python 2 directory for packages installing. Use `pip3` instead.*
3. Install dependencies:
    * `pip install -U scikit-learn`
    * `pip install librosa`
    * `brew install ffmpeg` on OS X

### How To ###
0. *Optional.* Resample audiofiles to 11025 sample rate. `python3 bin/resample.py audios_dir` **Warning:** script resamples audio files inplace.

1. Extract features from resampled audio. `python3 bin/extract_features.py audios_dir annotations_dir features_dir`

2. Train data with extracted features and annotations. `python3 bin/train.py features_dir trained_models_dir`

3. Decode audio. `python3 bin/decode.py audio_path trained_models_dir`

### Decoding ###
To decode audio file run `python3 bin/decode.py audio_path trained_models_dir` in Terminal

### Links ###
* [Automatic Chord Estimation from Audio: A Review of the State of the Art](http://www.mattmcvicar.com/wp-content/uploads/2014/01/taslp-mcvicar-2294580-proof_color.pdf) - Short summary of ACE task approaches.
* [A Machine Learning Approach to Automatic Chord Extraction](http://www.mattmcvicar.com/wp-content/uploads/2014/02/thesis.pdf) - Good summary of ACE approaches and music theory.
* [Chord Recognition python notebook](https://github.com/dpwe/elene4896/blob/master/prac10/e4896_prac10_chords.ipynb)
* [Beat-synchronized feature extraction notebook](https://github.com/dpwe/elene4896/blob/master/prac10/e4896_beat_sync_chroma.ipynb)
* [HPA](https://arxiv.org/pdf/1107.4969.pdf) - well explained BSN model

### Dataset ###
[Annotations list](http://www.audiocontentanalysis.org/data-sets/)

### TODO ###
* [HMM package](https://github.com/jmschrei/yahmm/wiki) - package for HMM with custom topology
* [TO READ](https://qmro.qmul.ac.uk/xmlui/bitstream/handle/123456789/451/MAUCHAutomaticChord2010.pdf?sequence=1)
* [TO READ](http://matthiasmauch.de/_pdf/mauch_sec_2010.pdf)