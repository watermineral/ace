import os.path as path
import os
import librosa.core


names = ['N',
         'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B',
         'C:min', 'C#:min', 'D:min', 'D#:min', 'E:min', 'F:min', 'F#:min', 'G:min', 'G#:min', 'A:min', 'A#:min', 'B:min']


def chord(index):
    return names[index]


def index(chord):
    return names.index(chord)


def load(path):
    chords = []
    with open(path) as file:
        for line in file:
            start, end, chord = line.split(' ')
            chords.append((librosa.core.time_to_frames(float(start), sr=11025)[0],
                           librosa.core.time_to_frames(float(end), sr=11025)[0],
                           index(parse(chord))))
    return chords


def load_all(basepath, filenames):
    chords = {ch_f:load(path.join(basepath, path.splitext(ch_f)[0] + str('.lab'))) for ch_f in filenames}
    return chords


def parse(chord_str):
    result = chord_str[0]
    if len(chord_str) > 1:
        if chord_str[1] == 'b':
            result = chord(index(chord_str[0])-1)
        if chord_str[1] == '#':
            result += '#'
        if 'min' in chord_str:
            result += ':min'
    return result
