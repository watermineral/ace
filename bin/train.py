import chord as ch
import wav
import chromogram_extraction.preprocessor as che
import hmm.training
import sklearn.externals.joblib
import pickle
import sys
import numpy as np
import os.path as path
import os
os.environ['LIBROSA_CACHE_DIR'] = '/tmp/librosa_cache'


def read_features_and_chromas(path):
    chromagrams = {}
    beats = {}
    labels = {}
    files = [f for f in os.listdir(path) if f.endswith(".pkl")]
    for file in files:
        with open(os.path.join(path, file), "rb") as f:
            beats[file], chromagrams[file], labels[file] = pickle.load(f)
    return beats, chromagrams, labels


def train(features_path, models_directory):
    print('Reading features and chromas from ', features_path)
    beats, chromagrams, labels = read_features_and_chromas(features_path)

    print('Start training')
    flatten_labels = []
    flatten_chromagrams = []
    for name, track_labels in labels.items():
        flatten_labels.extend(track_labels)
        flatten_chromagrams.extend(chromagrams[name])
    flatten_chromagrams = np.array(flatten_chromagrams)
    flatten_labels = np.array(flatten_labels)

    valid_indexes = np.apply_along_axis(lambda a: np.isfinite(a).all(), 1, flatten_chromagrams)
    flatten_chromagrams = flatten_chromagrams[valid_indexes]
    flatten_labels = flatten_labels[valid_indexes]

    # Train models
    models = hmm.training.train_chord_models(flatten_chromagrams, flatten_labels)
    transitions = hmm.training.train_transitions(flatten_labels)
    priors = hmm.training.train_prior(labels.values())
    print('Training finished')
    print('Saving trained models')
    for i, model in enumerate(models):
        sklearn.externals.joblib.dump(model, path.join(models_directory, "model" + str(i) + ".pkl"))
    with open(path.join(models_directory, 'transitions.pkl'), 'wb') as file:
        pickle.dump(transitions, file)
    with open(path.join(models_directory, 'priors.pkl'), 'wb') as file:
        pickle.dump(priors, file)
    print('Done')

train(sys.argv[1], sys.argv[2])