import chord as ch
import wav
import chromogram_extraction.preprocessor as che
import pickle
import os.path as path
import os
import librosa.feature
import sys


audios_path, labels_path, features_path = sys.argv[1], sys.argv[2], sys.argv[3]
print('Reading audios from ', audios_path)
audios = wav.load_all(audios_path)
print('Reading chords from ', labels_path)
timed_chords = ch.load_all(labels_path, audios.keys())

print('Start feature extraction')
chromagrams = {}
beats = {}
labels = {}
for name, audio in audios.items():
    print(name)
    chromagrams[name], beats[name] = che.extract(audio)

    # Extract chromagrams with the default chroma_cqt method
    #beats[name] = che.beats(audio)
    #chromagrams[name] = che.beat_sync_chroma(librosa.feature.chroma_cqt(audio, sr=11025), beats[name]).T

    # remove unlabeled features
    while beats[name][-1] > timed_chords[name][-1][1]:
        beats[name] = beats[name][:-1]
        chromagrams[name] = chromagrams[name][:-1]

print('Start beat synchronization')
# label each beat with chord which covers most of beat's time
for name, track_beats in beats.items():
    chord_index = 0
    chords = timed_chords[name]
    track_labels = []
    start, end, chord = chords[chord_index]
    for beat_index in range(0, len(track_beats) - 1):
        if track_beats[beat_index + 1] <= end:
            track_labels.append(chord)
        else:
            chord_index += 1
            chord_time = end - track_beats[beat_index]
            next_chord_time = track_beats[beat_index + 1] - end
            if chord_time > next_chord_time:
                track_labels.append(chord)
                start, end, chord = chords[chord_index]
            else:
                start, end, chord = chords[chord_index]
                track_labels.append(chord)
    labels[name] = track_labels
print('Done')

def write_beat_chroma_labels(filename, beat_times, chroma_features, label_indices):
    """Write out the computed beat-synchronous chroma data."""

    # Create the enclosing directory if needed.
    directory = os.path.dirname(filename)
    if directory and not os.path.exists(directory):
        os.makedirs(directory)
    with open(filename, "wb") as f:
        pickle.dump((beat_times, chroma_features, label_indices), f, pickle.HIGHEST_PROTOCOL)

for name in beats:
    beats[name] = librosa.core.samples_to_time(beats[name], sr=11025)[1:]

for name in beats:
    write_beat_chroma_labels(path.join(features_path,path.splitext(path.split(name)[-1])[0] + ".pkl"), beats[name], chromagrams[name], labels[name])
