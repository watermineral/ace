import sklearn.externals.joblib
import pickle
import chord as ch
import librosa.core
import wav
import chromogram_extraction.preprocessor as che
import hmm.decoding as decoder
import sys
import os.path as path
import numpy as np
import sklearn.mixture.gaussian_mixture as gm


def decode(audio_file, models_directory):
    """Print timed list of chord of audio_file"""
    print('Loading models from ', models_directory)
    models = np.zeros((25,), dtype=gm.GaussianMixture)
    for i in range(0, 25):
        models[i] = sklearn.externals.joblib.load(path.join(models_directory, 'model' + str(i) + '.pkl'))
    with open(path.join(models_directory, 'transitions.pkl'), 'rb') as file:
        transitions = pickle.load(file)
    with open(path.join(models_directory, 'priors.pkl'), 'rb') as file:
        priors = pickle.load(file)

    print('Loading audio ', audio_file)
    audio = wav.read(audio_file)

    print('Extracting features from audio')
    chromagram, beats = che.extract(audio)

    print('Decoding')
    chords = decoder.decode(chromagram, models, transitions, priors)
    print('Done')

    for i, chord in enumerate(chords):
        print(librosa.core.frames_to_time(beats[i], sr=11025), librosa.core.frames_to_time(beats[i+1], sr=11025),ch.chord(chord))

decode(sys.argv[1], sys.argv[2])