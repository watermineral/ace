import wav
import librosa.core
import librosa.output
import os.path
import sys


def resample(directory):
    """Resample all audio files in directory to sample rate of 11025Hz"""
    audios = wav.load_all(directory)
    for filename, audio in audios.items():
        file = os.path.join(directory, filename)
        librosa.output.write_wav(file, audio, sr=11025)


resample(sys.argv[1])