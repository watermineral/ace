import numpy as np


# Method from:
# https://github.com/dpwe/elene4896/blob/master/prac10/e4896_prac10_chords.ipynb
def viterbi_path(posteriors, transitions, priors):
    num_frames, num_states = posteriors.shape
    traceback = np.zeros((num_frames, num_states), dtype=int)
    # Normalized best probability-to-date for each state.
    best_prob = priors * posteriors[0]
    best_prob /= np.sum(best_prob)
    for frame in range(1, num_frames):
        # Find most likely combination of previous prob-to-path, and
        # transition.
        possible_transition_scores = (transitions *
                                      np.outer(best_prob, posteriors[frame]))
        # The max is found for each destination state (column), so the max
        # is over all the possible preceding states (rows).
        traceback[frame] = np.argmax(possible_transition_scores, axis=0)
        best_prob = np.max(possible_transition_scores, axis=0)
        best_prob /= np.sum(best_prob)
    # Traceback from best final state to get best path.
    path = np.zeros(num_frames, dtype=int)
    path[-1] = np.argmax(best_prob)
    for frame in range(num_frames - 1, 0, -1):
        path[frame - 1] = traceback[frame, path[frame]]
    return path


def decode(chromagram, models, transitions, priors):
    scores = np.array([model.score_samples(chromagram) for model in models])
    chords = viterbi_path(np.exp(scores.T), transitions, priors)
    return chords