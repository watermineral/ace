import numpy as np
import sklearn.mixture


def train_chord_models(flatten_chromograms, flatten_labels):
    global_model = sklearn.mixture.GaussianMixture()
    global_model.fit(flatten_chromograms)
    models = []
    # 24 chords + NoChordLabel
    for i in range(0, 25):
        chord_indexes = np.nonzero(flatten_labels == i)
        if len(chord_indexes[0]):
            model = sklearn.mixture.GaussianMixture()
            model.fit(flatten_chromograms[chord_indexes])
            models.append(model)
        else:
            models.append(global_model)
    return models


def train_transitions(flatten_labels):
    transitions_list = 100*flatten_labels[:-1] + flatten_labels[1:]
    transitions = np.zeros((25, 25))
    for i in range(0, 25):
        for j in range(0, 25):
            current_transition = i * 100 + j
            # 1 cause transition should have at least some prob
            transitions[i,j] = 1 + np.count_nonzero(transitions_list == current_transition)
    total = np.sum(transitions, axis=1)
    transitions /= total[:, np.newaxis]
    return transitions


def train_prior(labels):
    priors = np.zeros((25,))
    for track_label in labels:
        priors[track_label[0]] += 1
    priors /= np.sum(priors[:])
    return priors
