import librosa.core
import librosa.decompose
import librosa.feature
import librosa.beat
import numpy


def cqt(signal):
    """Constant-Q transform of audio signal"""
    return librosa.core.cqt(signal, sr=11025)


def beats(signal):
    """Return signal's beats bounds"""
    bpm, beat_track = librosa.beat.beat_track(signal, sr=11025)
    return beat_track


def perceptual_weighting(spectrogram):
    """Return loud-based spectrogram"""
    #cqt_frequencies = librosa.cqt_frequencies(n_bins=84, fmin=librosa.note_to_hz('C1'))
    #return librosa.core.perceptual_weighting(spectrogram**2, cqt_frequencies)
    return librosa.core.logamplitude(spectrogram**2)


def octave_summed_loudness_matrix(spectrogram):
    """Sum loudness of all octaves of each pitch class"""
    oslm = numpy.zeros((12,spectrogram.shape[1]))
    for t in range(0, spectrogram.shape[1] - 1):
        for j in range(0, spectrogram.shape[0] - 1):
            pitch_class = j % 12
            oslm[pitch_class, t] += spectrogram[j, t]
    return oslm


def beat_sync_chroma(spectrogram, beats):
    """Return beat-synchronized spectrogram"""
    bs_chroma = numpy.empty((12, len(beats) - 1))

    # uncomment next line if assertion error appears
    #while numpy.any(numpy.isnan(bs_chroma)):
    for t in range(0, len(beats) - 1):
        bs_chroma[:,t] = numpy.mean(spectrogram[:,beats[t]:beats[t+1]], axis=1)
    mins = numpy.amin(bs_chroma, axis=0)
    maxs = numpy.amax(bs_chroma, axis=0)
    for t in range(0, bs_chroma.shape[1]):
        if maxs[t] != mins[t]:
            bs_chroma[:,t] = (bs_chroma[:,t] - mins[t])/(maxs[t] - mins[t])
        else:
            bs_chroma[:,t] = 0
    # Because sometimes things here goes wrong and NaN appears in bs_chroma after median calculating
    nans = numpy.argwhere(numpy.isnan(bs_chroma))
    if len(nans):
        print(nans)
    assert not numpy.any(numpy.isnan(bs_chroma))
    return bs_chroma



def extract(signal):
    """Return chromas and beats of track"""
    cqt_spectrogram = cqt(signal)
    pw_spectrogram = perceptual_weighting(cqt_spectrogram)
    beats_track = beats(signal)
    beats_track = numpy.insert(beats_track, [0, beats_track.size], [0, pw_spectrogram.shape[1] - 1])
    only_unique_beats_track = []
    for i in range(len(beats_track) - 1):
        if beats_track[i] != beats_track[i + 1]:
            only_unique_beats_track.append(beats_track[i])
    only_unique_beats_track.append(beats_track[-1])
    oslm = octave_summed_loudness_matrix(pw_spectrogram)
    chromagram = beat_sync_chroma(oslm, only_unique_beats_track)
    return (chromagram.T, only_unique_beats_track)